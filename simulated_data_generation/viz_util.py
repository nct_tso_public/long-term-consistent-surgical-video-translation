import open3d as o3d
import torch

def make_cam_frame(cam_pose,xyz):

	t_cam = cam_pose[:3].view(1,3)
	corners_cam = torch.stack([
		xyz[0,0],
		xyz[0,-1],
		xyz[-1,0],
		xyz[-1,-1]
	])
	corners_cam = t_cam + ((corners_cam-t_cam) / torch.norm(corners_cam-t_cam,dim=1,keepdim=True) * 10)
	points = torch.cat((t_cam,corners_cam),dim=0)

	lines = [
		[0,1],
		[0,2],
		[0,3],
		[0,4],
		[1,2],
		[1,3],
		[2,4],
		[3,4],
	]
	colors = [[1, 0, 0] for i in range(len(lines))]
	cam_frame = o3d.geometry.LineSet(
		points=o3d.utility.Vector3dVector(points),
		lines=o3d.utility.Vector2iVector(lines),
	)
	cam_frame.colors = o3d.utility.Vector3dVector(colors)

	return cam_frame

def make_unit_frame():

	points = [
		[0, 0, 0],
		[1, 0, 0],
		[0, 1, 0],
		[1, 1, 0],
		[0, 0, 1],
		[1, 0, 1],
		[0, 1, 1],
		[1, 1, 1],
	]
	lines = [
		[0, 1],
		[0, 2],
		[1, 3],
		[2, 3],
		[4, 5],
		[4, 6],
		[5, 7],
		[6, 7],
		[0, 4],
		[1, 5],
		[2, 6],
		[3, 7],
	]
	colors = [[1, 0, 0] for i in range(len(lines))]
	line_set = o3d.geometry.LineSet(
		points=o3d.utility.Vector3dVector(points),
		lines=o3d.utility.Vector2iVector(lines),
	)
	line_set.colors = o3d.utility.Vector3dVector(colors)
	
	return line_set