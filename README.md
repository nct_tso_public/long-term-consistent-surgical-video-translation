
# Long-Term Temporally Consistent Unpaired Video Translation from Simulated Surgical 3D Data

This is the official implementation of our ICCV '21 [paper](https://arxiv.org/abs/2103.17204) for unpaired synthesis of view-consistent surgical video sequences.

![example image](example.png)

**NOTE:** This repository is intended for training your **own model** on your **own synthetic and real data**. If you want to download our **translated dataset** of realistic views and videos with 3D ground truth, visit our [project page](http://opencas.dkfz.de/video-sim2real/).

## Setup

Two different environments are required:
- Environment No.1 for generating training data: PyTorch- & OpenCV-enabled blender
- Environment No.2 for training: A simple PyTorch env

Environment No.1 for generating training data:
> [Blender 2.79b](https://www.blender.org/download/releases/2-79/) with ```torch``` and ```opencv-python``` installed in Blender's [local python](https://blender.stackexchange.com/questions/56011/how-to-install-pip-for-blenders-bundled-python) (for generating training data).

Environment No.2 for training:
> `pytorch`, `torchvision`, `tensorboardx`

<details><summary>Detailed setup:</summary>

I used `Ubuntu 18.04.6 LTS`. Setup for Windows or Mac might be different.

#### Environment No.1 for generating training data:

1. Download Blender2.79b [here](https://www.blender.org/download/releases/2-79/) (which uses Python 3.5.3) and unpack.
2. `cd <path_to_blender>/2.79/python`
3. run `bin/python3.5m lib/python3.5/ensurepip`
4. run `bin/pip3 install --target lib/python3.5 torch==1.5.1`
5. run `bin/pip3 install --target lib/python3.5 opencv-contrib-python==4.2.0.34`

#### Environment No.2 for training:

Training does not seem to be very version dependent and only requires a conda environment with Pytorch, Torchvision and TensorboardX installed:

```powershell
conda create -n videotranslation
conda install pytorch torchvision pytorch-cuda=11.8 -c pytorch -c nvidia
conda install -c conda-forge tensorboardx
```

Specifically, this configuration works on my system (`Python 3.11.5`, `pytorch=2.1.1`, `torchvision=0.16.1`, `tensorboardx=2.6.2.2`):

```yaml
name: videotranslation
channels:
  - pytorch
  - nvidia
  - conda-forge
  - defaults
dependencies:
  - _libgcc_mutex=0.1=main
  - _openmp_mutex=5.1=1_gnu
  - blas=1.0=mkl
  - brotli-python=1.0.9=py311h6a678d5_7
  - bzip2=1.0.8=h7b6447c_0
  - ca-certificates=2023.11.17=hbcca054_0
  - certifi=2023.11.17=pyhd8ed1ab_0
  - cffi=1.16.0=py311h5eee18b_0
  - charset-normalizer=2.0.4=pyhd3eb1b0_0
  - cryptography=41.0.3=py311hdda0065_0
  - cuda-cudart=11.8.89=0
  - cuda-cupti=11.8.87=0
  - cuda-libraries=11.8.0=0
  - cuda-nvrtc=11.8.89=0
  - cuda-nvtx=11.8.86=0
  - cuda-runtime=11.8.0=0
  - ffmpeg=4.3=hf484d3e_0
  - filelock=3.13.1=py311h06a4308_0
  - freetype=2.12.1=h4a9f257_0
  - giflib=5.2.1=h5eee18b_3
  - gmp=6.2.1=h295c915_3
  - gmpy2=2.1.2=py311hc9b5ff0_0
  - gnutls=3.6.15=he1e5248_0
  - idna=3.4=py311h06a4308_0
  - intel-openmp=2023.1.0=hdb19cb5_46306
  - jinja2=3.1.2=py311h06a4308_0
  - jpeg=9e=h5eee18b_1
  - lame=3.100=h7b6447c_0
  - lcms2=2.12=h3be6417_0
  - ld_impl_linux-64=2.38=h1181459_1
  - lerc=3.0=h295c915_0
  - libcublas=11.11.3.6=0
  - libcufft=10.9.0.58=0
  - libcufile=1.8.1.2=0
  - libcurand=10.3.4.101=0
  - libcusolver=11.4.1.48=0
  - libcusparse=11.7.5.86=0
  - libdeflate=1.17=h5eee18b_1
  - libffi=3.4.4=h6a678d5_0
  - libgcc-ng=11.2.0=h1234567_1
  - libgomp=11.2.0=h1234567_1
  - libiconv=1.16=h7f8727e_2
  - libidn2=2.3.4=h5eee18b_0
  - libjpeg-turbo=2.0.0=h9bf148f_0
  - libnpp=11.8.0.86=0
  - libnvjpeg=11.9.0.86=0
  - libpng=1.6.39=h5eee18b_0
  - libprotobuf=3.20.3=he621ea3_0
  - libstdcxx-ng=11.2.0=h1234567_1
  - libtasn1=4.19.0=h5eee18b_0
  - libtiff=4.5.1=h6a678d5_0
  - libunistring=0.9.10=h27cfd23_0
  - libuuid=1.41.5=h5eee18b_0
  - libwebp=1.3.2=h11a3e52_0
  - libwebp-base=1.3.2=h5eee18b_0
  - llvm-openmp=14.0.6=h9e868ea_0
  - lz4-c=1.9.4=h6a678d5_0
  - markupsafe=2.1.1=py311h5eee18b_0
  - mkl=2023.1.0=h213fc3f_46344
  - mkl-service=2.4.0=py311h5eee18b_1
  - mkl_fft=1.3.8=py311h5eee18b_0
  - mkl_random=1.2.4=py311hdb19cb5_0
  - mpc=1.1.0=h10f8cd9_1
  - mpfr=4.0.2=hb69a4c5_1
  - mpmath=1.3.0=py311h06a4308_0
  - ncurses=6.4=h6a678d5_0
  - nettle=3.7.3=hbbd107a_1
  - networkx=3.1=py311h06a4308_0
  - numpy=1.26.0=py311h08b1b3b_0
  - numpy-base=1.26.0=py311hf175353_0
  - openh264=2.1.1=h4ff587b_0
  - openjpeg=2.4.0=h3ad879b_0
  - openssl=3.0.12=h7f8727e_0
  - packaging=23.2=pyhd8ed1ab_0
  - pillow=10.0.1=py311ha6cbd5a_0
  - pip=23.3.1=py311h06a4308_0
  - protobuf=3.20.3=py311h6a678d5_0
  - pycparser=2.21=pyhd3eb1b0_0
  - pyopenssl=23.2.0=py311h06a4308_0
  - pysocks=1.7.1=py311h06a4308_0
  - python=3.11.5=h955ad1f_0
  - pytorch=2.1.1=py3.11_cuda11.8_cudnn8.7.0_0
  - pytorch-cuda=11.8=h7e8668a_5
  - pytorch-mutex=1.0=cuda
  - pyyaml=6.0.1=py311h5eee18b_0
  - readline=8.2=h5eee18b_0
  - requests=2.31.0=py311h06a4308_0
  - setuptools=68.0.0=py311h06a4308_0
  - sqlite=3.41.2=h5eee18b_0
  - sympy=1.11.1=py311h06a4308_0
  - tbb=2021.8.0=hdb19cb5_0
  - tensorboardx=2.6.2.2=pyhd8ed1ab_0
  - tk=8.6.12=h1ccaba5_0
  - torchtriton=2.1.0=py311
  - torchvision=0.16.1=py311_cu118
  - typing_extensions=4.7.1=py311h06a4308_0
  - tzdata=2023c=h04d1e81_0
  - urllib3=1.26.18=py311h06a4308_0
  - wheel=0.41.2=py311h06a4308_0
  - xz=5.4.2=h5eee18b_0
  - yaml=0.2.5=h7b6447c_0
  - zlib=1.2.13=h5eee18b_0
  - zstd=1.5.5=hc292b87_0
```

</details>

## Data Preparation

**NOTE:** Unfortunately, cannot share the real images or the synthetic 3D scenes due to the use of non-public patient data or licence restrictions, respectively. This repository contains only **dummies for real and synthetic data** in order to provide a minimal, executable code base.

### Synthetic Data (Domain A in the paper):

Generates synthetic images and render data (texture-pixel correspondences, interpolation weights) from synthetic, abdominal 3D scenes. The dummy 3D scenes (Blender files) are located in `simulated_data_generation/`.
- [simulated_data_generation/ExampleScene_FakeLiver_0.blend](simulated_data_generation/ExampleScene_FakeLiver_0.blend)
- [simulated_data_generation/ExampleScene_FakeLiver_1.blend](simulated_data_generation/ExampleScene_FakeLiver_1.blend).

#### Training data:
```powershell
cd simulated_data_generation
bash gen_random.sh
```

Synthetic reference images and render data (texture-pixel correspondences, interpolation weights) for each scene will be written to `data/simulated/`.

#### Test data:
```powershell
cd simulated_data_generation
bash gen_seq.sh
```

This will be written to `data/simulated_sequences/`. Note that the `train.py` script translates and saves test images every N iterations. So test data has to be generated before training.

<details><summary>Tips for using your own 3D scenes:</summary>

- It might be easiest to start out with the dummy scenes and **successively edit** them to your need, instead of replacing them entirely.
- The **names of objects/organs** in your Blender scene have to be specified in [simulated_data_generation/raycastTensified.py#L24](simulated_data_generation/raycastTensified.py#L24).
- For generating **random views (training)**, the Blender object [CameraPositionVolume](simulated_data_generation/renderRandomImages.py#L140) defines the space of possible camera positions. Further, the camera always [points towards a random point](simulated_data_generation/renderRandomImages.py#L175) (`lookTarget`) inside the **liver**. In your scene, you might have to define the `lookTarget` differently.
- For generating **sequences (testing)**, camera position and look target are obtained from manually defined [keyframes](simulated_data_generation/raycastTensified.py?#L249). These can most easily be set in the Blender UI.
- Ideally, height and width of synthetic images should be a power of 2 (due to down- and upsampling in the generator networks). With the current hyperparameters (n_downsample=2), height and width being divisible by 4 is sufficient. Set `img_width` and `img_height` for synthetic images in [simulated_data_generation/renderRandomImages.py](simulated_data_generation/renderRandomImages.py) and [simulated_data_generation/renderSequences.py](simulated_data_generation/renderSequences.py). Additionally, you will also have to adjust `crop_image_height` and `crop_image_width` for real images in [translation_model/configs/simulation2surgery.yaml](translation_model/configs/simulation2surgery.yaml), aswell.

</details>

### Real Data (Domain B in the paper):

Simply replace the dummy data in `data/real/` with your own dataset of real images. The folder structure does not matter. The dataloader loads every file with an image extension in this directory or its subdirectories.

*Note:* No video sequences are required. The real dataset can contain arbitrary, non-consecutive images.

## Training

After synthetic data was generated, run the following to train the model:
```powershell
cd translation_model
python3 train.py --output_path trials/test_trial
```

Checkpoints will be written to `trials/test_trial`, including examples of random translation samples to `trials/test_trial/outputs/simulation2surgery/images`. Per checkpoint, a grid of samples is saved as a single image, where columns represent random samples and rows represent input, reconstructions and translations as follows:

![example output](output_example.png)

In the current settings, checkpoints and samples are only written every 10k iterations. For getting started and debugging, this can be set to a lower number [here](translation_model/configs/simulation2surgery.yaml#L2)

## Inference

After training, translate synthetic video sequences by running:
```powershell
cd translation_model
python3 translate.py --input_folder [PATH_TO_SYNTH_SEQUENCES] --output_folder [OUT_PATH] --checkpoint [PATH_TO_GENERATOR_CHECKPOINT] 
```

E.g.:
```powershell
cd translation_model
python3 translate.py --input_folder ../data/simulated_sequences --output_folder translations/test_trial --checkpoint trials/test_trial/outputs/simulation2surgery/checkpoints/gen_00000010.pt
```
(Note: The test scenes in `data/simulated_sequences` can be a subset of the training scenes in `data/simulated`. However, the names and order of scenes in [translation_model/translate.py#L52](https://gitlab.com/nct_tso_public/surgical-video-sim2real/-/blob/master/translation_model/translate.py?ref_type=heads#L52) should exactly match your training scenes in `data/simulated` since this list is used to match learned textures to scenes.)

## 3D Visualization

Use the following script to visualize the 3D view and check the code to see the format of e.g. **depth, pose or 3D coordinates**:
```powershell
cd simulated_data_generation
python3 viz_3d_view.py
```
(requires Open3d)

## Citation

This work was presented at the [IEEE/CVF International Conference on Computer Vision 2021](https://iccv2021.thecvf.com/home). If you use this code, please cite our paper:

```bibtex
@InProceedings{Rivoir_2021_ICCV,
    author    = {Rivoir, Dominik and Pfeiffer, Micha and Docea, Reuben and Kolbinger, Fiona and Riediger, Carina and Weitz, J\"urgen and Speidel, Stefanie},
    title     = {Long-Term Temporally Consistent Unpaired Video Translation From Simulated Surgical 3D Data},
    booktitle = {Proceedings of the IEEE/CVF International Conference on Computer Vision (ICCV)},
    month     = {October},
    year      = {2021},
    pages     = {3343-3353}
}
```

This work was carried out at the National Center for Tumor Diseases (NCT) Dresden, [Department of Translational Surgical Oncology](https://www.nct-dresden.de/tso.html) and the Centre for Tactile Internet ([CeTI](https://ceti.one/)) at TU Dresden.

## Acknowledgements

Funded by the German Research Foundation (DFG, Deutsche Forschungsgemeinschaft) as part of Germany’s Excellence Strategy – EXC 2050/1 – Project ID 390696704 – Cluster of Excellence “Centre for Tactile Internet with Human-in-the-Loop” (CeTI) of Technische Universität Dresden.

## License

Licensed under the CC BY-NC-SA 4.0 license (https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode). 

Note that this is a direct modification of Pfeiffer et al.'s ([Paper](https://arxiv.org/abs/1907.02882), [GitLab](https://gitlab.com/nct_tso_public/laparoscopic-image-2-image-translation/)) work and the MUNIT framework ([Paper](https://arxiv.org/abs/1804.04732), [GitHub](https://github.com/NVlabs/MUNIT)). The original Copyright holder is NVIDIA Corporation:

Copyright (C) 2018 NVIDIA Corporation.  All rights reserved.
Licensed under the CC BY-NC-SA 4.0 license (https://creativecommons.org/licenses/by-nc-sa/4.0/legalcode). 

Additionally, we use the MS-SSIM implementation (```translation_model/pytorch_msssim``` folder) by Jorge Pessoa ([GitHub](https://github.com/jorge-pessoa/pytorch-msssim)) which is licensed under the MIT license.

These licenses allow you to use, modify and share the project for non-commercial use as long as you adhere to the conditions of the license above.

## Contact

If you have any questions, do not hesitate to contact us: ```dominik.rivoir [at] nct-dresden.de```
